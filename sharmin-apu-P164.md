| Filename                     | Size     |
|------------------------------|----------|
| VLSI - Sharmin.pdf           | 29871 KB |

### Topics and Page number
 - Ratioless NMOS inverter - 51
 - Ratioless clocked load dynamin flip flop - 83
 - Ratioed and ratioless design - 41
 - Generation of IC - 01
 - what is vlsi electronics - 02
 - nmos pass transistor - 48
 - power dissisipation - 120
 - PLA, PLD, PAL - from 155
 - pseudo static RAM - 150
 - three transistor dynamic ram - 148
 - 
