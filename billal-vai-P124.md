| Filename                     | Size     |
|------------------------------|----------|
| VLSI 2018-12-11 20.24.26.pdf | 16973 KB |

### Topics and Page number
 - Dynamic CMOS Logic - 83
 - Parity Generator - 87
 - Example of Structural design (Combinational Logic) - 87
 - Four way MUX, NMOS - 91
 - Selection operation of 4 way MUX with truth table - 91
 - vlsi, microelectronics - 3
 - components of vlsi - 3
 - design approaches - 5
 - top down design hierarchy full custom design - 7
 - moore's law - 9
 - embedded system - 9
 - mos design and basic circuits, semiconductors, intrinsic, extrinsic - 10
 - mos structure - 11
 - threshold voltage - 13
 - enhancement mode NMOS transistor - 14
 - depletion mode NMOS transistor - 15
 - body effect - 15
 - ids vs vds characteristic NMOS device - 16
 - resistive region - 17
 - saturation region - 20
 - characteristic equation for PMOS device - 20
 - why nmos devices are preferred, preference cmos - 21
 - priciple of inverters - 22
 - nmos inverter with resistive load - 23
 - nmos inverter with nmos enhancement transistor load - 24
 - nmos inverter with nmos enhancement transistor load limitation solution - 26
 - nmos inverter with nmos depletion transistor load - 27
 - edge time nmos inverter with nmos depletion transistor load - 28
 - ratioed ratioless design - 32
 - importenece of aspect ratio - 32
 - cmos inverter - 33
 - edge time in cmos fall time in nmos inverter - 35
 - characteristic curve of cmos inverter - 35
 - i vs vout curve for t1 and t2 - 35
 - nmos pass transistor - 37
 - ratioless nmos inverter - 38
 - cmos pass gate - 39
 - buffer circuits - 41
 - problems to derive large number of gate with the output of inverter - 41
 - super buffer - 43
 - stick diagram - 44
 - fabrication and design rules - 47
 - n-well cmos inverter fabrication - 47
 - flow designs, main steps in a typical n-well process - 49
 - p-well CMOS inverter fabrication - 50
 - nmos inverter with depletion load fabrication - 51
 - nmos inverter with enhancement load fabrication - 52 (bottom)
 - twin-tub process for CMOS inverter fabrication, limitation - 53
 - twin-tub process for CMOS inverter fabrication, limitation, solution - 55
 - BiCmos inverter is faster than mos, why use bicmos - 55
 - comparison of mos and bicmos, mos vs bicmos - 56
 - purpose of design rule - 57
 - lambda based design rules - 57
 - difference between lambda based and miyu based / m based - 58
 - contact cuts, butting contacts, buried contacts, buried contaact is preferred over butting contact - 59
 - geometric layout - 60
 - geometric layout nmos inverter with nmos depletion load - 60
 - geometric layout nmos inverter - 61
 - stick diagram examples - 62
 - chapter 4 mos logical circuit design - 63
 - not use nand gate with fan in greater than 4 - 63
 - draw and explain following using nmos - 63
 - static flip-flop, s-r flip flop - 64
 - d flip flop - 65
 - j k flip flop - 67
 - why master and slave structure - 67
 - wwhy two phase non overlapping clock - 67
 - dynamic flip flop - 69
 - three transistor 
 - PLA, PLD, PAL - from 116
 - FPGA - 121
 - DRAM vs SRAM - 115
